package com.huilan.base.esl.pojo.mq;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEvent {


    private String eventName;
    private String eventSubclass;
    private String ext;
    private Stats stats;

    public enum Stats{
        unknown,
        register,
        unregister;
    }

    public static UserEvent getInstance(Map<String,String> eventHeaders){
        String name = eventHeaders.get("Event-Name").toLowerCase();
        String subclass = eventHeaders.get("Event-Subclass").toLowerCase();
        String ext = eventHeaders.get("from-user");
        Stats stats = "sofia::register".equals(subclass) ? Stats.register : ("sofia::unregister".equals(subclass) ? Stats.unregister : Stats.unknown);
        return UserEvent.builder()
                .eventName(name)
                .eventSubclass(subclass)
                .ext(ext)
                .stats(stats)
                .build();
    }

}
