package com.huilan.base.esl.pojo.fs;

/**
* @Description: esl事件处理
* @Param:
* @return:
* @Author: liuwc
* @Date:   2020/9/21 14:26
*/
public enum EslEventEnum {
    channel_create("创建","channelListener",true),
    channel_park("暂停","channelListener",false),
    channel_answer("接听","channelListener",true),
    channel_hangup_complete("挂机","channelListener",true),

    background_job("后台任务","backgroundJobListener",true),

    heartbeat("心跳事件","heartbeatListener",true),

    sofia_register("分机上线","sofiaListener",true,"sofia::register","custom"),
    sofia_unregister("分机下线","sofiaListener",true,"sofia::unregister","custom"),

    conference_maintenance("会议","conferenceListener",true,"conference::maintenance","custom"),

    ;


    //事件描述
    private String description;
    //事件处理beanName
    private String beanName;
    //事件处理对象
    private Object bean;
    //是否处理当前事件
    private boolean isHandle = false;
    //自定义事件
    private String subclass;
    private String type;

    EslEventEnum() {
    }

    EslEventEnum(String description, String beanName) {
        this.description = description;
        this.beanName = beanName;
    }

    EslEventEnum(String description, String beanName, boolean isHandle) {
        this.description = description;
        this.beanName = beanName;
        this.isHandle = isHandle;
    }

    EslEventEnum(String description, String beanName, boolean isHandle, String subclass) {
        this.description = description;
        this.beanName = beanName;
        this.isHandle = isHandle;
        this.subclass = subclass;
    }

    EslEventEnum(String description, String beanName, boolean isHandle, String subclass, String type) {
        this.description = description;
        this.beanName = beanName;
        this.isHandle = isHandle;
        this.subclass = subclass;
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public boolean isHandle() {
        return isHandle;
    }

    public void setHandle(boolean handle) {
        isHandle = handle;
    }


    public String getSubclass() {
        return subclass;
    }

    public void setSubclass(String subclass) {
        this.subclass = subclass;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
