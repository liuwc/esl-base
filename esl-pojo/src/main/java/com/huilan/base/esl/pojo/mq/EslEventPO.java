/*
 * Copyright 2010 david varnes.
 *
 * Licensed under the Apache License, version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at:
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.huilan.base.esl.pojo.mq;

import com.huilan.base.esl.pojo.fs.EslEventEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EslEventPO implements Serializable {

    private Map<String,String> eventHeaders;
    private List<String> eventBody;


    public enum EventType{
        channel,
        background_job,
        user,
        conference,
        ;
    }

    /**
     * @Description  获取事件类型
     * @param
     * @Return com.huilan.base.esl.pojo.mq.EslEventPO.EventType
     * @Author liuwc
     * @Date 2021/4/28 9:28
    */
    public EventType getEventType(){
        String eventName = getEventName().toLowerCase();
        if ("background_job".equals(eventName)){
            return EventType.background_job;
        }else if(eventName.contains("channel")){
            return EventType.channel;
        }else if("custom".equals(eventName)){
            String eventSubclass = getEventSubclass().toLowerCase();
            if ("sofia::register".equals(eventSubclass) || "sofia::unregister".equals(eventSubclass)){
                return EventType.user;
            }else if("conference::maintenance".equals(eventSubclass)){
                return EventType.conference;
            }
        }
        return EventType.channel;
    }
    // 获取分机事件
    public UserEvent getUserEvent(){
        return UserEvent.getInstance(eventHeaders);
    }
    // 获取会议事件
    public ConferenceEvent getConferenceEvent(){
        return ConferenceEvent.getInstance(eventHeaders);
    }

    // 事件名
    public String getEventName(){
        return get("Event-Name");
    }
    // 获取自定义事件子类名
    public String getEventSubclass(){
        return get("Event-Subclass");
    }
    // 事件枚举
    public EslEventEnum getEventNameEnum(){
        String eventName = getEventName();
        return EslEventEnum.valueOf(eventName);
    }
    //挂机原因
    public String getHangupCause(){
        return get("Hangup-Cause");
    }
    // 通话时长
    public String getDuration(){
        return get("variable_duration");
    }
    // 不同版本的fs Timestamp 的长度可能不一样
    public String getTimeStamp(){
        return get("Event-Date-Timestamp");
    }
    // 获取自定义通道变量
    public String getCustom(String key){
        return get("variable_" + key);
    }
    public String getUuid(){
        return get("Unique-ID");
    }
    public String getOtherLegUuid(){
        return get("Other-Leg-Unique-ID");
    }

    public String getJobUuid(){
        return get("Job-UUID");
    }
    // 获取主叫号码
    public String getCallerNumber(){
        return get("Caller-Caller-ID-Number");
    }
    // 被叫号码
    public String getCalleeNumber(){
        return get("Caller-Destination-Number");
    }
    // 是否 freeswitch挂断
    public Boolean isFsHangup(){
        String variable_sip_hangup_disposition = get("variable_sip_hangup_disposition");
        if (variable_sip_hangup_disposition == null || variable_sip_hangup_disposition.length()==0){
            variable_sip_hangup_disposition = get("variable_verto_hangup_disposition");
            if (variable_sip_hangup_disposition == null || variable_sip_hangup_disposition.length()==0){
                return null;
            }
        }
        return variable_sip_hangup_disposition.contains("send");
    }

    // sip 状态码
    public String getSipStatus(){
        return get("variable_sip_invite_failure_status");
    }

    public String get(String key){
        return this.getEventHeaders().get(key);
    }
}
