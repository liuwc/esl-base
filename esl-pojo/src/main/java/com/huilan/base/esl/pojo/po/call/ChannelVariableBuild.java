package com.huilan.base.esl.pojo.po.call;


import com.huilan.base.esl.pojo.ChannelVariableBase;

import java.util.HashMap;
import java.util.Map;

public class ChannelVariableBuild extends ChannelVariableBase {

    /**
     * @Description 构建两条腿的通道变量
     * @param mapA  a腿 通道变量
	 * @param mapB
	 * @param mapSip     sip参数
	 * @param recordUrl 录音地址
	 * @param onlyAudio true语音电话  false视频电话
	 * @param caller_number 主叫
	 * @param caller_name
	 * @param record_concat_video  合成视频
     *                              todo  通道变量的长度有限制  暂无控制
     * @Return java.util.Map<java.lang.String,java.util.Map<java.lang.String,java.lang.String>>
     * @Author liuwc
     * @Date 2021/5/13 13:53
    */
    public static Map<String,Map<String,String>> buildCustomAndRecord2(Map<String,String> mapA,Map<String,String> mapB,Map<String,String> mapSip,String recordUrl,boolean onlyAudio,String caller_number,String caller_name,boolean record_concat_video){
        if (mapSip != null && mapSip.size()>0){
            buildSipChannelVariable(mapSip)
                    .entrySet()
                    .forEach(s->{
                        mapA.put(s.getKey(),s.getValue());
                        mapB.put(s.getKey(),s.getValue());
                    });
        }
        // 立体音
        mapA.put("RECORD_STEREO","true");
        // 执行录音
        if (recordUrl != null && recordUrl.length()>0){
            mapA.put("execute_on_media","'record_session "+recordUrl+"'");
        }
        if (record_concat_video){
            mapA.put("record_concat_video","true");
        }
        if (caller_name != null){
            mapA.put("origination_caller_id_name",caller_name);
        }
        if (caller_number != null){
            mapA.put("origination_caller_id_number",caller_number);
        }
        if (onlyAudio){
            // 仅包含PCMA,PCMU媒体类型 适用于仅语音呼叫 不会触发视频的媒体协商
            mapA.put("absolute_codec_string","PCMA,PCMU");
            mapB.put("absolute_codec_string","PCMA,PCMU");
        }
        Map<String,Map<String,String>> map = new HashMap<>();
        map.put("a",mapA);
        map.put("b",mapB);
        return map;
    }

}
