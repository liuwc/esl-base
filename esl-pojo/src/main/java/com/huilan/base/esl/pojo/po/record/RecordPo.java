package com.huilan.base.esl.pojo.po.record;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecordPo {

    private String uuid;
    // 录音地址
    private String recordAddress;
    // 是否异步录音
    private boolean isRecordSession;
    // 通道变量
    private RecordChannelVariable recordChannelVariable;
}
