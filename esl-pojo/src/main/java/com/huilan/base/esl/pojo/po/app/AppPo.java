package com.huilan.base.esl.pojo.po.app;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppPo {

    private String uuid;
    private String name;
    private String arg;


}
