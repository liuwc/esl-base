package com.huilan.base.esl.pojo.mq;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChannelEvent {


    private String eventName;
    private String uuid;
    private String otherLegUuid;
    private String timeStamp;
    private String duration;
    private String hangupCause;
    private String jobUuid;
    private String callerNumber;
    private String calleeNumber;
    private String sipHangupDisposition;
    private String vertoHangupDisposition;
    private String sipStatus;


}
