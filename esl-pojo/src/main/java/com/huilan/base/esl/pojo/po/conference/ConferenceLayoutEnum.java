package com.huilan.base.esl.pojo.po.conference;

public enum ConferenceLayoutEnum {

    layout_1x1("1x1",""),
    layout_2x2("2x2",""),
    layout_3x3("3x3",""),
    layout_4x4("4x4",""),
    layout_5x5("5x5",""),
    layout_6x6("6x6",""),
    layout_8x8("8x8",""),

    layout_1x2("1x2",""),
    layout_2x1("2x1",""),
    layout_1x1_2x1("1x1+2x1",""),

    layout_2x1_zoom("2x1-zoom",""),
    layout_2x1_presenter_zoom("2x1-presenter-zoom",""),
    layout_3x1_zoom("3x1-zoom",""),
    layout_3x2_zoom("3x2-zoom",""),
    layout_4x2_zoom("4x2-zoom",""),

    layout_5_grid_zoom("5-grid-zoom",""),
    layout_7_grid_zoom("7-grid-zoom",""),

    layout_1up_top_left_5("1up_top_left+5",""),
    layout_1up_top_left_7("1up_top_left+7",""),
    layout_1up_top_left_9("1up_top_left+9",""),
    layout_2up_top_8("2up_top+8","顶部两个大的，下面8个小的"),
    layout_2up_bottom_8("2up_bottom+8","底部两个大的 "),
    layout_2up_middle_8("2up_middle+8","中间两个大的"),
    layout_3up_4("3up+4","3个大的 "),
    layout_3up_9("3up+9",""),

    layout_presenter_overlap_large_top_right("presenter-overlap-large-top-right","演讲者重叠右上方"),
    layout_presenter_overlap_large_bot_right("presenter-overlap-large-bot-right",""),
    layout_presenter_overlap_small_top_right("presenter-overlap-small-top-right",""),
    layout_presenter_overlap_small_bot_right("presenter-overlap-small-bot-right",""),
    layout_presenter_dual_horizontal("presenter-dual-horizontal","水平"),
    layout_presenter_dual_vertical("presenter-dual-vertical","垂直"),

    layout_overlaps("overlaps","叠加"),

    layout_default("group:grid","默认"),
    ;

    private String code;
    private String description;

    ConferenceLayoutEnum(String code) {
        this.code = code;
    }

    ConferenceLayoutEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
