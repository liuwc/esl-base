package com.huilan.base.esl.pojo.mq;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConferenceEvent {


    private String eventName;
    private String eventSubclass;
    private String conferenceId;
    private String memberId;
    private String user;
    private Action action;
    private String timestamp;

    public enum Action{
        unknown,
        conference_create,
        conference_destroy,
        add_member,
        del_member,
        energy_level,
        volume_level,
        gain_level	,
        dtmf,
        stop_talking,
        start_talking,
        mute_detect,
        mute_member,
        unmute_member,
        kick_member,
        dtmf_member,
        energy_level_member,
        volume_in_member,
        volume_out_member,
        play_file,
        play_file_done,
        play_file_member,
        lock,
        unlock,
        transfer,
        bgdial_result,
        floor_change,
        record,
        ;

    }


    public static ConferenceEvent getInstance(Map<String,String> eventHeaders){

        String name = eventHeaders.get("Event-Name").toLowerCase();
        String subclass = eventHeaders.get("Event-Subclass").toLowerCase();
        String conferenceName = eventHeaders.get("Conference-Name");
        String memberId = eventHeaders.get("Member-ID");
        String user = eventHeaders.get("Caller-Caller-ID-Number");
        String timestamp = eventHeaders.get("Event-Date-Timestamp");
        String action = eventHeaders.get("Action").replace("-","_");
        Action action1;
        try {
            action1 = Action.valueOf(action);
        }catch (Exception e){
            action1 = Action.unknown;
        }
        return ConferenceEvent.builder()
                .eventName(name)
                .timestamp(timestamp)
                .eventSubclass(subclass)
                .conferenceId(conferenceName)
                .memberId(memberId)
                .user(user)
                .action(action1)
                .build();
    }

}
