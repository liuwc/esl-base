package com.huilan.base.esl.pojo.po.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChannelPo {

    // 通道标识
    private String uuid;
    private String direction;
    private String created;
    private String created_epoch;
    private String name;
    private String state;
    private String cid_name;
    private String ip_addr;
    // 主叫号码
    private String cid_num;
    // 分机号
    private String dest;
    private String application;
    private String application_data;
    private String dialplan;
    private String context;
    private String read_codec;
    private String read_rate;
    private String read_bit_rate;
    private String write_codec;
    private String write_rate;
    private String write_bit_rate;
    private String secure;
    private String hostname;
    private String presence_id;
    private String presence_data;
    private String accountcode;
    private String callstate;
    private String callee_name;
    // 被叫号码
    private String callee_num;
    private String callee_direction;
    private String call_uuid;
    private String sent_callee_name;
    private String sent_callee_num;
    private String initial_cid_name;
    private String initial_cid_num;
    private String initial_ip_addr;
    private String initial_dest;
    private String initial_dialplan;
    private String initial_context;

    private Map<String,String> channelMap;


    public enum Type{
        unknown,
        call,
        conference,
        ;
    }
    public enum Callstate{
        unknown,
        ringing,
        answer,
        ;
    }

    public Type getType(){
        if (application == null || application.length()==0){
            return Type.unknown;
        }else if("conference".equals(application)){
            return Type.conference;
        }else{
            return Type.call;
        }
    }
    public Callstate getCallstateVo(){
        if (callstate == null || callstate.length()==0){
            return Callstate.unknown;
        }else if("RINGING".equals(callstate)){
            return Callstate.ringing;
        }else if("RING_WAIT".equals(callstate)){
            return Callstate.ringing;
        }else if("ACTIVE".equals(callstate)){
            return Callstate.answer;
        }else{
            return Callstate.unknown;
        }
    }


}
