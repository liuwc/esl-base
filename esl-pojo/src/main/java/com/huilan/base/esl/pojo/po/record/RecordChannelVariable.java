package com.huilan.base.esl.pojo.po.record;

import com.huilan.base.esl.pojo.AbstractChannelVariable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecordChannelVariable extends AbstractChannelVariable {


    // 只录"写"方向的录音。这里的方向是相对 FreeSWITCH而言的，即 FreeSWITCH发出，也就是对端能够听到的声音。
    private Boolean record_write_only;
    // 只录"读"方向的录音，即 FreeSWITCH"听"到的录音。口 RECORD STEREO∶ 立体声录音。
    private Boolean record_read_only;
    // 立体声录音
    private Boolean record_stereo;
    //  在默认情况下，如果指定的录音文件已经存在，再次录音就会 覆盖该文件。将该选项设为 true 可以向已经存在的文件后面追加录音。可用于电话中断重新建立后重新录音时。
    private Boolean record_append;


    // 默认值为 false，如果设置为 true 则会在录音失败 时（如文件不可写入、格式错误等）挂断电话。
    private Boolean record_hangup_on_error;
    // 同立体声录音，但将左、右声道互换。
    private Boolean record_stereo_swap;
    // 默认值为 false，如果设为 true，则在应答前执行了 record session 或 uuid record 就暂时不录音，而等到通话被应答后才开始录音。可以防止录上不必要的 Early Media（如回铃音或彩铃等）。
    private Boolean record_answer_req;
    // 与 record_answer_req类似，只是在当前的通道与 其他通道桥接了之后才开始录音。
    private Boolean record_bridge_req;
    // 可以在录音时指定采样率，以进行实时的转码。合法的采样率在 源代码中使用switch is valid rate（rate）宏定义，可能的取值有 8000、12000、16000、 24000、32000、11025、22050、44100、48000，单位为 Hz。
    private Boolean record_sample_rate;
    // 该变量的默认值为 true，即默认先写到一个内存缓冲区中，可以大大减小对存储设备（如硬盘）的访问次数，防止产生 IO 瓶颈。默认的缓冲区大小是65536 字节。
    private Boolean enable_file_write_buffering;
    // 最小录音秒数。如果录音时长小于该数（默认是 3 秒），则认为 是无效的录音，并删除录音文件。
    private Integer record_min_sec;
    // 如果从录音开始检测不到声音，则在指定的毫 秒后超时，停止录音。
    private Integer record_initial_timeout_ms;
    // 如果在录音过程中检测不到声音，则在指定的毫 秒后超时，停止录音。
    private Integer record_final_timeout_ms;
    // 静音能量阈值，即能量小于该值认为是静音。默认值为 200。
    private Integer record_silence_threshold;

}
