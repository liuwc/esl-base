package com.huilan.base.esl.pojo.po.api;



/**
 * @Description  https://freeswitch.org/confluence/display/FREESWITCH/mod_commands
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 11:53
*/
public enum ApiEnum {

    acl("Match acl IP address"),
    alias("设置别名"),
    domain_exists("检查一个FreeSWITCH域是否存在"),
    eval("在呼叫会话期间查看通道变量"),

    // expand bgapi originate sofia/internal/1001%${domain} 9999  -> bgapi originate sofia/internal/1001%192.168.1.1 9999
    expand("有变量的呼叫串最前面加上expand 会先赋值变量然后执行  加到 bgapi前面"),
    fsctl("Send control messages to FreeSWITCH.   "),
    global_getvar("获取全局变量的值。如果未提供参数，则它将获取所有全局变量。"),
    global_setvar("global_setvar <变量名> = <值>"),
    group_call("group_call group@domain[+F|+A|+E]"),
    create_uuid(""),

    originate("发起一个呼叫"),

    pause("暂停媒体播放"),
    uuid_answer("uuid_answer"),
    uuid_audio(""),
    uuid_break(""),
    uuid_bridge(""),
    uuid_broadcast(""),
    uuid_chat(""),
    uuid_exists(""),
    uuid_hold(""),
    uuid_kill(""),
    uuid_park(""),
    uuid_record("Record the audio associated with the channel with the given UUID into a file. "),
    uuid_transfer("Transfers an existing call to a specific extension within a <dialplan> and <context>. Dialplan may be \"xml\" or \"directory\"."),
    ;

    private String description;


    ApiEnum(String description) {
        this.description = description;
    }
}
