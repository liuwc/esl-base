package com.huilan.base.esl.pojo.po.conference;

public enum ConferenceEnum {

    list("查看会议成员","conference 3000 list | conference list"),
    energy("设置会议成员能量值","conference 3000 energy all/last/non_moderator/memberId 500"),
    volume_in("成员输入音量","conference 3000 volume_in 1 1"),
    volume_out("成员输出音量","conference 3000 volume_out 1 1"),
    play("播放声音文件","conference 3000 play /home/aa.wav | conference 3000 play /home/aa.wav 1"),
    pause("暂停播放，暂停录音"),
    resume("恢复"),
    file_seek("声音文件的快进，快退","conference 3000 file_seek [+|-]<val>"),
    file_vol("Changes the volume of the currently playing sound file","conference <confname> file-vol <val> [async]"),
    say(),
    saymember(),
    stop("停止播放","conference 3000 stop all/last/current"),
    dtmf("send dtmf"),
    kick("踢出成员","conference 3000 kick 1"),
    hup("挂断成员","conference 3000 hup 1"),
    mute("成员静音","conference 3000 hup 1"),
    tmute("切换成员静音状态","conference 3000 tmute 1"),
    unmute("取消成员静音状态","conference 3000 ummute 1"),
    deaf("禁听","conference 3000 deaf 1"),
    undeaf("取消禁听","conference 3000 undeaf 1"),
    // conference <confname> relate <member_id>[,<member_id>] <other_member_id>[,<other_member_id>] [nospeak|nohear|clear|sendvideo]
    relate("单独对某个成员静音","conference 3000 relate 1 2 nospeak # 1听不到2 | conference 3000 relate 1 2 nohear # 2听不到1 | conference 3000 relate 1 2 clear # 清除关系"),
    lock("锁定会议,无法加入"),
    unlock(),
    agc("启用自动增益控制 Auto Gain Control AGC可以自动调麦克风的收音量，使与会者收到一定的音量水平，不会因发言者与麦克风的距离改变时，声音有忽大忽小声的缺点",
            "Query level: conference 3000 agc | conference 3000 agc off | conference 3000 agc on | conference 3000 agc on 1120"),
    dial("从会议呼出","conference 3000 dial user/1001  |  conference 3000 dial user/1001 liuwc 1001 "),
    bgdial("异步 dial"),
    transfer("将 member 1 转到另一个会议","conference 3000 transfer 3001 1"),
    record("录音","conference 3000 record /home/aa.wav"),
    chkrecord("检查录音情况"),
    norecord("停止录音"),
    recording("在会议的Profile中设置了录音，可以控制录音","conference 3000 recording start/stop/check/pause/resume"),
    exit_sound("退会音","conference 3000 exit_sound on/off/none |  conference 3000 exit_sound file a.wav"),
    enter_sound("入会音","conference 3000 enter_sound on/off/none |  conference 3000 enter_sound file a.wav"),
    pin("修改密码","conference 3000 pin 12346"),
    nopin("取消密码","conference 3000 nopin"),
    get("获取参数的值"),
    set(),
    floor("将某成员设置成floor 没什么用"),
    // fs 1.6
    vid_floor("视频成员","vid-floor    # 用中划线"),
    // fs 1.6
    clear_vid_floor("","clear-vid-floor    # 用中划线"),
    ;



    private String description;
    private String example;

    ConferenceEnum() {
    }

    ConferenceEnum(String description) {
        this.description = description;
    }

    ConferenceEnum(String description, String example) {
        this.description = description;
        this.example = example;
    }
}
