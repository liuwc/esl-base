package com.huilan.base.esl.pojo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiResult<T> {


    // 返回状态码
    private Integer code;
    // 说明
    private String msg;
    // 用户说明
    private String msg_vo;
    // 数据
    private T v;



    public boolean isOk(){
        return code == 200;
    }

}
