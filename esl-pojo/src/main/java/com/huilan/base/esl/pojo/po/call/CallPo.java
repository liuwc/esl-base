package com.huilan.base.esl.pojo.po.call;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CallPo {

    private String uuid;
    private String name;
    private String arg;


}
