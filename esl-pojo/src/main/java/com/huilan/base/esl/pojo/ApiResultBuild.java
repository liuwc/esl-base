package com.huilan.base.esl.pojo;

public class ApiResultBuild {

    public enum  Code{
        ok(200,"ok","操作成功"),
        fail(500,"fail","操作失败");
        private int code;
        private String msg;
        private String msgVO;
        Code(int code, String msg,String msgVO) {
            this.code = code;
            this.msg = msg;
            this.msgVO = msgVO;
        }
    }

    public static ApiResult success(){
        return ApiResult
                .builder()
                .code(Code.ok.code)
                .msg(Code.ok.msg)
                .msg_vo(Code.ok.msgVO)
                .build();
    }
    public static ApiResult success(Object object){
        return ApiResult
                .builder()
                .code(Code.ok.code)
                .msg(Code.ok.msg)
                .msg_vo(Code.ok.msgVO)
                .v(object)
                .build();
    }

    public static ApiResult fail(){
        return ApiResult
                .builder()
                .code(Code.fail.code)
                .msg(Code.fail.msg)
                .msg_vo(Code.fail.msgVO)
                .build();
    }
    public static ApiResult fail(int code){
        return ApiResult
                .builder()
                .code(code)
                .msg(Code.fail.msg)
                .msg_vo(Code.fail.msgVO)
                .build();
    }
    public static ApiResult fail(String msg_vo){
        return ApiResult
                .builder()
                .code(Code.fail.code)
                .msg(Code.fail.msg)
                .msg_vo(msg_vo)
                .build();
    }

    public static ApiResult fail(int code,String msg){
        return ApiResult
                .builder()
                .code(code)
                .msg(msg)
                .msg_vo(msg)
                .build();
    }
    public static ApiResult fail(int code,String msg,String msgVO){
        return ApiResult
                .builder()
                .code(code)
                .msg(msg)
                .msg_vo(msgVO)
                .build();
    }

    
}
