package com.huilan.base.esl.pojo.po.chat;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatPo {

    // 协议
    private String protocol;
    // 消息发送者 fs系统用户 0000000000
    private String sender;
    // 消息接收者
    private String receiver;
    // 内容
    private String content;


}
