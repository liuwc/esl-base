package com.huilan.base.esl.pojo.po.conference;


import com.huilan.base.esl.pojo.ChannelVariableBase;
import com.huilan.base.esl.pojo.po.call.ChannelVariableBuild;

import java.util.HashMap;
import java.util.Map;

public class ConferenceChannelVariableBuild extends ChannelVariableBase {




    private static Map<String,Map<String,String>> buildChannelVariable(Map<String,String> mapA,Map<String,String> mapSip){
        buildSipChannelVariable(mapSip)
                .entrySet()
                .forEach(s->{
                    mapA.put(s.getKey(),s.getValue());
                });
        Map<String,Map<String,String>> map = new HashMap<>();
        map.put("a",mapA);
        return map;
    }

    public static Map<String,Map<String,String>> buildChannelVariable(Map<String,String> mapSip){
        Map<String,String> map = new HashMap<>();
        return buildChannelVariable(map,mapSip);
    }
    /**
     * @Description  会议通道变量自定义主叫
     * @param mapSip    sip map
	 * @param caller_number 主叫号码
	 * @param caller_name 主叫姓名
     * @Return java.util.Map<java.lang.String,java.util.Map<java.lang.String,java.lang.String>>
     * @Author liuwc
     * @Date 2021/4/15 11:42
    */
    public static Map<String,Map<String,String>> buildChannelVariable(Map<String,String> mapSip,String caller_number,String caller_name){
        Map<String,String> map = new HashMap<>();
        map.put("origination_caller_id_name",caller_name);
        map.put("origination_caller_id_number",caller_number);
        return buildChannelVariable(map,mapSip);
    }
     public static Map<String,Map<String,String>> buildChannelVariable(Map<String,String> mapA,Map<String,String> mapSip,String caller_number,String caller_name,boolean mute){
         if (mute){
             mapA.put("conference_member_flags","mute");
         }
        mapA.put("origination_caller_id_name",caller_name);
        mapA.put("origination_caller_id_number",caller_number);
        return buildChannelVariable(mapA,mapSip);
    }




}
