package com.huilan.base.esl.pojo.po.app;

public enum AppEnum {

    answer("接听"),
    bridge("桥接"),
    hangup("挂机"),
    hold("呼叫保持"),
    unhold("取消呼叫保持"),
    lua("执行lua"),
    event("发一个事件"),
    send_dtmf("Send dtmf to be sent"),
    set("Set a channel variable"),
    unset("UnSet a channel variable"),
    multiset("Set many channel variables"),
    park("暂停"),
    playback("播放"),
    push("Set a channel variable"),
    record("同步录音"),
    record_session("异步录音"),
    stop_record_session("异步录音停止"),
    rename("Rename file"),
    intercept("强插"),
    ;



    private String description;


    AppEnum(String description) {
        this.description = description;
    }

}
