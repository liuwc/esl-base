package com.huilan.base.esl.pojo;


import java.util.HashMap;
import java.util.Map;

public class ChannelVariableBase {


    protected static Map<String,String> buildSipChannelVariable(Map<String,String> mapSip){
        if (mapSip == null || mapSip.size()==0){
            return null;
        }
        Map<String, String> map = new HashMap<>();
        mapSip.entrySet().forEach(s->{
            map.put("sip_h_"+s.getKey(),s.getValue());
        });
        return map;
    }

}
