package com.huilan.base.esl.pojo.po.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventPo {

    // 事件名称
    private String eventName;
    // 事件头
    private Map<String,Object> header;
    // 正文
    private String body;


}
