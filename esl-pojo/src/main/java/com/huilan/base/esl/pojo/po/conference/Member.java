package com.huilan.base.esl.pojo.po.conference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @Description fs 会议成员
 * @Return
 * @Author liuwc
 * @Date 2021/3/21 16:10
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Member {

    private String memberId;
    // 呼叫串
    private String callStr;
    private String uuid;
    // 分机号
    private String user;
}
