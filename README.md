# esl-base

#### 介绍
1. 这是一个简单的单机版 freeswitch java esl 服务 
2. 目前正在不断完善fs常用的app和api，包含 呼叫 会议 录音 视频 等等
3. fs集群版方案正在完善
4. 源码里的连接问题，高并发查询数据错乱等问题已经修复


#### 软件架构
1. 仅包含org.freeswitch.esl.client:0.9.2源码


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  微信

![image](weichat.png)  



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
