package com.huilan.base.esl.service.event;

import lombok.extern.slf4j.Slf4j;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.Client;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ConferenceListener extends BaseListener implements IEslEventListener {
    @Override
    public void eventReceived(EslEvent event, Client client) {
        String memberId = event.getEventHeaders().get("Member-ID");
        String user = event.getEventHeaders().get("Caller-Caller-ID-Number");
        String action = event.getEventHeaders().get("Action");
//        log.info("user:{} memberId:{} action:{} header:{}",user,memberId,action,event.getEventHeaders());
        sendMq(event);
    }
}
