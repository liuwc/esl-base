package com.huilan.base.esl.service.acd.domain.model.queue.acd;

import com.huilan.base.esl.service.acd.domain.model.agent.Agent;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Random;

@Component
public class RandomACD implements IQueueACD {

    @Override
    public Agent choose(List<Agent> list) {
        int index = new Random().nextInt(list.size());
        return list.get(index);
    }
}
