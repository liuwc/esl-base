package com.huilan.base.esl.service.redis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HashRO {

    private String key;
    private String item;
    private Object t;
}
