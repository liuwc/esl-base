package com.huilan.base.esl.service.acd.domain.model.agent.event;

import com.huilan.base.esl.service.acd.domain.model.agent.Agent;
import com.huilan.base.esl.service.acd.domain.model.queue.acd.Loop;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AgentListener {

    @EventListener
    public void online(AgentOnlineEvent agentOnlineEvent){

        Agent agent = agentOnlineEvent.getAgent();
        log.info("agent:{} 收到上线事件",agent.getExt());

        Loop.flushNo(agent.getExt());
        log.info("agent:{} 刷新loopACD no",agent.getExt());
    }




}
