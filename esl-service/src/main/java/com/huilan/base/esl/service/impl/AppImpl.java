package com.huilan.base.esl.service.impl;

import com.huilan.base.esl.api.AppApi;
import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.ApiResultBuild;
import com.huilan.base.esl.pojo.po.app.AppEnum;
import com.huilan.base.esl.pojo.po.app.AppPo;
import com.huilan.base.esl.service.fs.EslClient;
import lombok.extern.slf4j.Slf4j;
import org.freeswitch.esl.client.transport.CommandResponse;
import org.freeswitch.esl.client.transport.SendMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class AppImpl extends BaseImpl implements AppApi {

    @Override
    public ApiResult app(AppPo appPo) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(appPo))
                .getResponse()
                .getBodyLines());
    }
    private SendMsg sendMsg(AppPo appPo){
        SendMsg sendMsg = new SendMsg(appPo.getUuid());
        sendMsg.addCallCommand("execute");
        sendMsg.addExecuteAppName(appPo.getName());
        if (appPo.getArg() != null){
            sendMsg.addExecuteAppArg(appPo.getArg());
        }
        return sendMsg;
    }
    @Override
    public ApiResult answer(String uuid) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.answer.name())
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult bridge(String uuid, String user) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.bridge.name())
                        .arg("user/"+user)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult hangup(String uuid) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.hangup.name())
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult hold(String uuid) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.hold.name())
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult unhold(String uuid) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.unhold.name())
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult lua(String uuid, String fileAddress) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.lua.name())
                        .arg(fileAddress)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult set(String uuid, String arg) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.set.name())
                        .arg(arg)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult unset(String uuid, String arg) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.unset.name())
                        .arg(arg)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult multiset(String uuid, String arg) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.multiset.name())
                        .arg(arg)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult park(String uuid) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.park.name())
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult playback(String uuid, String fileAddress) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.playback.name())
                        .arg(fileAddress)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult record(String uuid, String recordAddress) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.record.name())
                        .arg(recordAddress)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult record_session(String uuid, String recordAddress) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.record_session.name())
                        .arg(recordAddress)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult stop_record_session(String uuid, String recordAddress) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.stop_record_session.name())
                        .arg(recordAddress)
                        .build()))
                .getResponse()
                .getBodyLines());
    }

    @Override
    public ApiResult intercept(String uuid, String uuidB) {
        return ApiResultBuild.success(eslClient
                .getClient()
                .sendMessage(sendMsg(AppPo
                        .builder()
                        .uuid(uuid)
                        .name(AppEnum.intercept.name())
                        .arg(uuidB)
                        .build()))
                .getResponse()
                .getBodyLines());
    }
}
