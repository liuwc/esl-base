package com.huilan.base.esl.service.acd.domain.model.phoneinline.acd;

import com.huilan.base.esl.service.acd.domain.model.phoneinline.PhoneInline;

import java.util.List;

public interface IPhoneinlineACD {

    public PhoneInline choose(List<PhoneInline> phoneInlineList);

    public void init(String phone);

}
