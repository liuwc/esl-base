package com.huilan.base.esl.service.acd.domain.model.phoneinline;


import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.List;

import static com.huilan.base.esl.service.acd.domain.model.Base.getEslClient;

@Data
public class Channel {

    private String uuid;

    public Channel() {
    }

    public Channel(String uuid) {
        this.uuid = uuid;
    }

    public boolean calling(){
        List<String> stringList = getEslClient().getClient().sendAsyncApiCommand("uuid_exists", uuid);
        if (stringList != null && stringList.size()>0 && StrUtil.equalsIgnoreCase("true",stringList.get(0))){
            return true;
        }
        return false;
    }



}
