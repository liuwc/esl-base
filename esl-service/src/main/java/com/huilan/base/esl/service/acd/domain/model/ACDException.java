package com.huilan.base.esl.service.acd.domain.model;


public class ACDException extends RuntimeException{

    private String msg;

    public ACDException(String msg) {
        this.msg = msg;
    }

    public ACDException() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
