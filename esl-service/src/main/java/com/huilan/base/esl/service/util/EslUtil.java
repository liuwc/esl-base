package com.huilan.base.esl.service.util;

import java.util.Map;

public class EslUtil {


    public static String mapToChannelValue(Map<String, String> map) {
        if (map == null || map.size() == 0) {
            return "";
        }
        String str = "{";
        for (Map.Entry entry : map.entrySet()) {
            str += "," + entry.getKey() + "=" + checkValue(entry.getValue());
        }
        str = str.replaceFirst(",", "");
        str += "}";
        return str;
    }

    public static String mapToChannelValueBySetM(Map<String, String> map) {
        if (map == null || map.size() == 0) {
            return "";
        }
        String str = "";
        for (Map.Entry entry : map.entrySet()) {
            str += ";" + entry.getKey() + "=" + checkValue(entry.getValue());
        }
        str = str.replaceFirst(";", "");
        return str;
    }


    private static String checkValue(Object value){
        if (value == null)
            return "";
        String string = String.valueOf(value);
        if (string.contains(","))
            string = string.replace(",","\\,");
        return string;
    }
}
