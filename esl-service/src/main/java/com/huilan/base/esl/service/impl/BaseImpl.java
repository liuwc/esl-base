package com.huilan.base.esl.service.impl;

import com.huilan.base.esl.service.fs.EslClient;
import com.huilan.base.esl.service.redis.IdRedis;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
public class BaseImpl {

    @Autowired
    public EslClient eslClient;

    @Autowired
    public IdRedis idRedis;


    public String getHost(){
        return eslClient.getHost("0");
    }

    public List<String> commandSync(String command){
        return command(command,false);
    }
    public String commandAsync(String command){
        return command(command,true).get(0);
    }
    public List<String> command(String command,boolean skipResult){
        return eslClient.getClient().sendAsyncApiCommand(command, "",skipResult);
    }


}
