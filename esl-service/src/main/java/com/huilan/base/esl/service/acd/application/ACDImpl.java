package com.huilan.base.esl.service.acd.application;

import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.ApiResultBuild;
import com.huilan.base.esl.service.acd.domain.model.agent.Agent;
import com.huilan.base.esl.service.acd.domain.model.phoneinline.Channel;
import com.huilan.base.esl.service.acd.domain.model.phoneinline.PhoneInline;
import com.huilan.base.esl.service.acd.domain.model.queue.Queue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Service
@RestController
public class ACDImpl implements ACDApi{


    public void agentOnline(String ext){
        Agent agent = Agent.get(ext);
        if (agent != null){
            log.info("agent:{} 坐席挂机了",ext);
            agent.online(true);
        }

    }

    @Override
    public ApiResult transfer(TransferVO transferVO){
        log.info("transferVO:{}",transferVO);
        Queue queue = Queue.get(transferVO.getQueueId());
        PhoneInline phoneInline = PhoneInline.create(transferVO.getPhone(), new Channel(transferVO.getUuid()), queue);
        phoneInline.transfer();

        return ApiResultBuild.success();
    }

    @Override
    public ApiResult createQueue(String queueId, String acd) {
        return null;
    }

    @Override
    public ApiResult delQueue(String queueId) {
        return null;
    }

    @Override
    public ApiResult updateQueueACD(String queueId, String acd) {
        return null;
    }

    @Override
    public ApiResult createAgent(String agentId, String ext) {
        return null;
    }

    @Override
    public ApiResult delAgent(String agentId) {
        return null;
    }

    @Override
    public ApiResult updateAgentExt(String agentId, String ext) {
        return null;
    }

    @Override
    public ApiResult agentIntoQueue(String agentId, String queueId) {
        return null;
    }

    @Override
    public ApiResult agentOutQueue(String agentId, String queueId) {
        return null;
    }
}
