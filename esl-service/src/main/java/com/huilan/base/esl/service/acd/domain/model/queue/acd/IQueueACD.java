package com.huilan.base.esl.service.acd.domain.model.queue.acd;

import com.huilan.base.esl.service.acd.domain.model.agent.Agent;

import java.util.List;

public interface IQueueACD {

    public Agent choose(List<Agent> list);
}
