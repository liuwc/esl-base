package com.huilan.base.esl.service.acd.domain.model.phoneinline.acd;

import com.huilan.base.esl.service.acd.domain.model.phoneinline.PhoneInline;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class WaitACD implements IPhoneinlineACD{

    @Override
    public PhoneInline choose(List<PhoneInline> phoneInlineList) {

        Comparator<PhoneInline> comparator = (a,b) -> {
            Long difference = Wait.get(a.getPhone()).getNo() - Wait.get(b.getPhone()).getNo();
            return difference.intValue();
        };

        return phoneInlineList
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList())
                .get(0);
    }

    @Override
    public void init(String phone) {
        Wait.create(phone);
    }
}
