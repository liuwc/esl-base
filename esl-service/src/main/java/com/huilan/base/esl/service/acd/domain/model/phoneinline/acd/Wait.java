package com.huilan.base.esl.service.acd.domain.model.phoneinline.acd;

import lombok.Getter;
import lombok.Setter;
import org.redisson.api.RLiveObjectService;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;

import static com.huilan.base.esl.service.acd.domain.model.Base.getRedisson;

@Getter
@Setter
@REntity
public class Wait {

    @RId
    private String phone;
    private long no;

    private static final String no_key = "temp:phoneInline:wait:no";


    public Wait(long no) {
        this.no = no;
    }

    public Wait() {
    }

    public Wait(String phone, long no) {
        this.phone = phone;
        this.no = no;
    }

    public static long createNo(){
        return getRedisson().getAtomicLong(no_key).addAndGet(1);
    }


    public static Wait create(String phone){
        RLiveObjectService liveObjectService = getRedisson().getLiveObjectService();
        Wait wait = get(phone);
        if (wait != null){
            liveObjectService.delete(wait);
        }
        return liveObjectService.persist(new Wait(phone, createNo()));
    }

    public static Wait get(String phone){
        RLiveObjectService liveObjectService = getRedisson().getLiveObjectService();
        return liveObjectService.get(Wait.class, phone);
    }
    public static void delete(String phone){
        Wait wait = get(phone);
        if (wait != null){
            RLiveObjectService liveObjectService = getRedisson().getLiveObjectService();
            liveObjectService.delete(wait);
        }
    }



}
