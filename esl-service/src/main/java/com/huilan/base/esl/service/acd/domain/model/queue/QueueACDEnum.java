package com.huilan.base.esl.service.acd.domain.model.queue;

import com.huilan.base.esl.service.acd.domain.model.queue.acd.IQueueACD;

public enum QueueACDEnum {

    random("随机","randomACD"),
    loop("轮询","loopACD"),

    ;


    private String msg;
    private IQueueACD iacd;
    private String acdBeanName;

    QueueACDEnum() {
    }

    QueueACDEnum(String msg) {
        this.msg = msg;
    }

    QueueACDEnum(String msg, String acdBeanName) {
        this.msg = msg;
        this.acdBeanName = acdBeanName;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public IQueueACD getIacd() {
        return iacd;
    }

    public void setIacd(IQueueACD iacd) {
        this.iacd = iacd;
    }

    public String getAcdBeanName() {
        return acdBeanName;
    }

    public void setAcdBeanName(String acdBeanName) {
        this.acdBeanName = acdBeanName;
    }
}
