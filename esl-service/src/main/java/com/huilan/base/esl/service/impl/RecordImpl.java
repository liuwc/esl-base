package com.huilan.base.esl.service.impl;

import cn.hutool.core.util.StrUtil;
import com.huilan.base.esl.api.ApiApi;
import com.huilan.base.esl.api.AppApi;
import com.huilan.base.esl.api.ChatApi;
import com.huilan.base.esl.api.RecordApi;
import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.ApiResultBuild;
import com.huilan.base.esl.pojo.po.chat.ChatPo;
import com.huilan.base.esl.pojo.po.record.RecordPo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class RecordImpl extends BaseImpl implements RecordApi {

    @Autowired
    private AppApi appApi;
    @Autowired
    private ApiApi apiApi;

    @Override
    public ApiResult record(RecordPo recordPo) {
        Map<String, Object> map = recordPo.getRecordChannelVariable().transformChannelVariableMap();
        if (map.size()>0){
            apiApi.uuidSetvarMulti(recordPo.getUuid(),map);
        }
        if (recordPo.isRecordSession()){
            return appApi.record(recordPo.getUuid(),recordPo.getRecordAddress());
        }else{
            return appApi.record_session(recordPo.getUuid(),recordPo.getRecordAddress());
        }
    }
}
