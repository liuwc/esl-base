package com.huilan.base.esl.service.fs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties("fs")
public class FsConfig {
    private List<FsServer> server;
}
