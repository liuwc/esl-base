package com.huilan.base.esl.service.acd.domain.model.agent.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import com.huilan.base.esl.service.acd.domain.model.agent.Agent;

@Getter
public class AgentOnlineEvent extends ApplicationEvent {

    private Agent agent;

    public AgentOnlineEvent(Object source,Agent agent) {
        super(source);
        this.agent = agent;
    }

}
