package com.huilan.base.esl.service.acd.domain.model.agent;

import cn.hutool.core.util.StrUtil;

import java.util.List;

public enum  TransferFailedEnum {
    UNKNOWN("未知"),
    CALL_REJECTED("拒接"),
    NO_ANSWER("未接"),
    USER_NOT_REGISTERED("分机不在线"),
    ;


    private String description;


    TransferFailedEnum(String description) {
        this.description = description;
    }

    public static TransferFailedEnum getReason(List<String> reasons){
        if (reasons == null || reasons.size()==0){
            return TransferFailedEnum.UNKNOWN;
        }
        for (TransferFailedEnum transferFailedEnum:TransferFailedEnum.values()) {
            if (StrUtil.contains(reasons.get(0),transferFailedEnum.name())){
                return transferFailedEnum;
            }
        }
        return TransferFailedEnum.UNKNOWN;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
