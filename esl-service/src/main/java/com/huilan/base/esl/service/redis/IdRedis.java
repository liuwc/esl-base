package com.huilan.base.esl.service.redis;

import org.springframework.stereotype.Component;

@Component
public class IdRedis extends RedissonUtil {


    private static final String id_key = "db:id";


    /**
     * @Description  获取唯一id
     * @param
     * @Return java.lang.String
     * @Author liuwc
     * @Date 2021/3/21 14:36
    */
    public String get(){
        long incr = incr(id_key);
        return incr+"";
    }

}
