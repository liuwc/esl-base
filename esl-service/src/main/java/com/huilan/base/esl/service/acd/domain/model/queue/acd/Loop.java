package com.huilan.base.esl.service.acd.domain.model.queue.acd;

import static com.huilan.base.esl.service.acd.domain.model.Base.getRedisson;

public class Loop {

    private static final String no_key = "temp:agent:loop:no";
    private static final String base_no_key = "temp:agent:loop:no:base";


    public static void flushNo(String id){
        long no = addAndGetBaseNo();
        getRedisson().getMap(no_key).put(id,no);
    }

    public static long getNo(String id){
        Object o = getRedisson().getMap(no_key).get(id);
        if (o == null)
            return 0;
        return Long.parseLong(o+"");
    }

    public static long addAndGetBaseNo(){
        return getRedisson().getAtomicLong(base_no_key).addAndGet(1);
    }



}
