package com.huilan.base.esl.service.redis;

import cn.hutool.core.util.StrUtil;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class RedissonConfig {

    @Value("${redis.address}")
    private String address;
    @Value("${redis.password}")
    private String password;

    @Bean
    public RedissonClient redisson() throws IOException {
        // 本例子使用的是yaml格式的配置文件，读取使用Config.fromYAML，如果是Json文件，则使用Config.fromJSON
        Config config = Config.fromYAML(RedissonConfig.class.getClassLoader().getResource("redisson.yaml"));
        config.useSingleServer().setAddress(address).setPingConnectionInterval(1000);
        if (StrUtil.isNotEmpty(password)){
            config.useSingleServer().setPassword(password);
        }
        return Redisson.create(config);
    }
}
