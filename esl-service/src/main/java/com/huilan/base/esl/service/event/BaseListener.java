package com.huilan.base.esl.service.event;

import com.huilan.base.esl.pojo.mq.EslEventPO;
import com.huilan.base.esl.pojo.mq.Topic;
import com.huilan.base.esl.service.rabbitmq.RabbitUtil;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BaseListener {

    @Autowired
    private RabbitUtil rabbitUtil;


    public void sendMq(EslEvent event){

        EslEventPO eslEventPO = new EslEventPO(event.getEventHeaders(),event.getEventBodyLines());
        rabbitUtil.send(eslEventPO, Topic.FREESWITCH_EVENT);
    }

}
