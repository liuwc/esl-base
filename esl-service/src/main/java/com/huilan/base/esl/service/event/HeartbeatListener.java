package com.huilan.base.esl.service.event;

import lombok.extern.slf4j.Slf4j;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.Client;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class HeartbeatListener extends BaseListener implements IEslEventListener {


    @Override
    public void eventReceived(EslEvent event, Client client) {
        log.info("fs heartbeat");
    }


}
