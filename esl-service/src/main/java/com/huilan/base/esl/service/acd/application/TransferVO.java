package com.huilan.base.esl.service.acd.application;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransferVO {
    private String phone;
    private String uuid;
    private String queueId;

}
