package com.huilan.base.esl.service.acd.application;


import com.huilan.base.esl.pojo.ApiResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("esl/api/acd")
public interface ACDApi{

    @PostMapping("transfer")
    public ApiResult transfer(@RequestBody TransferVO transferVO);



    @PostMapping("queue/add")
    public ApiResult createQueue(@RequestParam("queueId") String queueId,@RequestParam("acd")String acd);
    @PostMapping("queue/del")
    public ApiResult delQueue(@RequestParam("queueId") String queueId);
    @PostMapping("queue/updateACD")
    public ApiResult updateQueueACD(@RequestParam("queueId") String queueId,@RequestParam("acd")String acd);



    @PostMapping("agent/add")
    public ApiResult createAgent(@RequestParam("agentId") String agentId,@RequestParam("ext") String ext);
    @PostMapping("agent/del")
    public ApiResult delAgent(@RequestParam("agentId") String agentId);
    @PostMapping("agent/updateExt")
    public ApiResult updateAgentExt(@RequestParam("agentId") String agentId,@RequestParam("ext") String ext);



    @PostMapping("agentIntoQueue")
    public ApiResult agentIntoQueue(@RequestParam("agentId") String agentId,@RequestParam("queueId") String queueId);
    @PostMapping("agentOutQueue")
    public ApiResult agentOutQueue(@RequestParam("agentId") String agentId,@RequestParam("queueId") String queueId);



}
