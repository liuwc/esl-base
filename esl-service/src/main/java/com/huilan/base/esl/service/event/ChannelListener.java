package com.huilan.base.esl.service.event;

import cn.hutool.core.util.StrUtil;
import com.huilan.base.esl.service.acd.application.ACDImpl;
import lombok.extern.slf4j.Slf4j;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.Client;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class ChannelListener extends BaseListener implements IEslEventListener {

    @Autowired
    private ACDImpl callCenterService;

    @Override
    public void eventReceived(EslEvent event, Client client) {

        sendMq(event);
        String eventName = event.getEventName();
        if (StrUtil.equalsIgnoreCase(eventName,"CHANNEL_HANGUP_COMPLETE")){
            Map<String, String> eventHeaders = event.getEventHeaders();
            String ext = eventHeaders.get("Caller-Destination-Number");
//            callCenterService.agentOnline(ext);
        }
    }


}
