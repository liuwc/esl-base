package com.huilan.base.esl.service.impl;

import cn.hutool.core.util.StrUtil;
import com.huilan.base.esl.api.ApiApi;
import com.huilan.base.esl.api.CallApi;
import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.ApiResultBuild;
import com.huilan.base.esl.pojo.po.api.ApiEnum;
import com.huilan.base.esl.pojo.po.api.ApiPo;
import com.huilan.base.esl.pojo.po.call.CallPo;
import com.huilan.base.esl.service.util.EslUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class CallImpl extends BaseImpl implements CallApi {

    @Override
    public ApiResult callAsync(CallPo callPo) {
        return null;
    }

    private ApiResult call(String callStr){
        List<String> stringList = eslClient.getClient().sendAsyncApiCommand("expand "+ ApiEnum.originate.name(), callStr, true);
        return ApiResultBuild.success(stringList.get(0));
    }

    @Override
    public ApiResult callUserAsync(String user) {
        return call(StrUtil.format("user/{} &park",user));
    }

    @Override
    public ApiResult callUserWithRecordAsync(String user, String recordAddress) {
        return call(StrUtil.format("{RECORD_STEREO=true,execute_on_media='record_session {}'}user/{} &park",recordAddress,user));
    }

    @Override
    public ApiResult callUser2UserAsync(String userA, String userB) {
        return call(StrUtil.format("user/{} &bridge(user/{})",userA,userB));
    }

    @Override
    public ApiResult callUser2UserWithChannelVariableAsync(String userA, String userB, Map<String,String> channelVariable) {
        String channelValue = EslUtil.mapToChannelValue(channelVariable);
        return call(StrUtil.format("{}user/{} &bridge(user/{})",channelValue,userA,userB));
    }

    @Override
    public ApiResult callUser2UserWithChannelVariable2Async(String userA, String userB, Map<String, Map<String, String>> channelVariable) {
        Map<String, String> mapA = channelVariable.get("a");
        Map<String, String> mapB = channelVariable.get("b");
        Map<String, String> mapSip = channelVariable.get("sip");
        String channelValueA = EslUtil.mapToChannelValue(mapA);
        String channelValueB = EslUtil.mapToChannelValue(mapB);
        return call(StrUtil.format("{}user/{} &bridge({}user/{})",channelValueA,userA,channelValueB,userB));
    }

    @Override
    public ApiResult callUser2UserWithRecordAsync(String userA, String userB, String recordAddress) {
        return call(StrUtil.format("{RECORD_STEREO=true,execute_on_media='record_session {}'}user/{} &bridge(user/{})",recordAddress,userA,userB));
    }

    @Override
    public ApiResult callUser2PhoneAsync(String user, String callStr) {
        return call(StrUtil.format("user/{} &bridge(sofia/gateway/{})",user,callStr));
    }

    @Override
    public ApiResult callUser2PhoneWithRecordAsync(String user, String callStr, String recordAddress) {
        return call(StrUtil.format("{RECORD_STEREO=true,execute_on_media='record_session {}'}user/{} &bridge(sofia/gateway/{})",recordAddress,user,callStr));
    }

    @Override
    public ApiResult callPhoneAsync(String callStr) {
        return call(StrUtil.format("sofia/gateway/{} &park",callStr));
    }

    @Override
    public ApiResult callPhoneWithRecordAsync(String callStr, String recordAddress) {
        return call(StrUtil.format("{RECORD_STEREO=true,execute_on_media='record_session {}'}sofia/gateway/{} &park",recordAddress,callStr));
    }

    @Override
    public ApiResult callPhone2PhoneAsync(String callStrA, String callStrB) {
        return call(StrUtil.format("sofia/gateway/{} &bridge(sofia/gateway/{})",callStrA,callStrB));
    }

    @Override
    public ApiResult callPhone2PhoneWithRecordAsync(String callStrA, String callStrB, String recordAddress) {
        return call(StrUtil.format("{RECORD_STEREO=true,execute_on_media='record_session {}'}sofia/gateway/{} &bridge(sofia/gateway/{})",recordAddress,callStrA,callStrB));
    }

    @Override
    public ApiResult callPhone2UserAsync(String callStr, String user) {
        return call(StrUtil.format("sofia/gateway/{} &bridge(user/{})",callStr,user));
    }

    @Override
    public ApiResult callPhone2UserWithRecordAsync(String callStr, String user, String recordAddress) {
        return call(StrUtil.format("{RECORD_STEREO=true,execute_on_media='record_session {}'}sofia/gateway/{} &bridge(user/{})",recordAddress,callStr,user));
    }
}
