package com.huilan.base.esl.service.exeception;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeoutException extends RuntimeException{

    private String msg;

}
