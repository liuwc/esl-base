package com.huilan.base.esl.service.event;

import lombok.extern.slf4j.Slf4j;
import org.freeswitch.esl.client.IEslEventListener;
import org.freeswitch.esl.client.inbound.Client;
import org.freeswitch.esl.client.transport.event.EslEvent;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SofiaListener extends BaseListener implements IEslEventListener {
    @Override
    public void eventReceived(EslEvent event, Client client) {
//        String user = event.getEventHeaders().get("from-user");
//        String sub = event.getEventHeaders().get("Event-Subclass");
//        log.info("user :{} {}",user,sub);
        sendMq(event);
    }
}
