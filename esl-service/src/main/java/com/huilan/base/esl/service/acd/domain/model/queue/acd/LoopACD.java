package com.huilan.base.esl.service.acd.domain.model.queue.acd;

import com.huilan.base.esl.service.acd.domain.model.agent.Agent;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LoopACD implements IQueueACD {

    @Override
    public Agent choose(List<Agent> agents) {

        Comparator<Agent> comparator = (a, b) -> {
            Long difference = Loop.getNo(a.getId()) - Loop.getNo(b.getId());
            return difference.intValue();
        };

        return agents
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList())
                .get(0);
    }
}
