package com.huilan.base.esl.service.fs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FsServer {

    private String id;
    private String host;
    private int port;
    private String password;
    private int timeout;

}
