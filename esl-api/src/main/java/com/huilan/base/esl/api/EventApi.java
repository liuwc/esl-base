package com.huilan.base.esl.api;

import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.event.EventPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description  事件  api
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 9:39
*/
@FeignClient("esl-service")
@RequestMapping("esl/api/event")
public interface EventApi {

    @PostMapping("send")
    public ApiResult send(@RequestBody EventPo eventPo);


    /**
     * @Description  发消息
     * @param user    分机号
	 * @param content   内容
     * @Return com.huilan.base.esl.pojo.ApiResult
     * @Author liuwc
     * @Date 2021/3/20 10:41
    */
    @PostMapping("send_message")
    public ApiResult send_message(@RequestParam("user") String user,@RequestParam("content")String content);


}
