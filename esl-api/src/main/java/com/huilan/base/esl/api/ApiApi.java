package com.huilan.base.esl.api;


import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.api.ApiPo;
import com.huilan.base.esl.pojo.po.app.AppPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Description   执行一个api
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 9:35
*/
@FeignClient("esl-service")
@RequestMapping("esl/api/api")
public interface ApiApi {

    // 执行一个api
    @PostMapping("api")
    public ApiResult api(@RequestBody ApiPo apiPo);
    @PostMapping("api/async")
    public ApiResult apiAsync(@RequestBody ApiPo apiPo);


    // 桥接会话
    @PostMapping("uuid_bridge")
    public ApiResult uuid_bridge(@RequestParam("uuidA") String uuidA,@RequestParam("uuidB") String uuidB);
    @PostMapping("uuid_bridge/async")
    public ApiResult uuid_bridgeAsync(@RequestParam("uuidA") String uuidA,@RequestParam("uuidB") String uuidB);

    //强插
    @PostMapping("intercept/async")
    public ApiResult interceptAsync(@RequestParam("uuid") String uuid,@RequestParam("user") String user);

    //查看所有的会话 返回 list<ChannelPo>
    @PostMapping("channels/list")
    public ApiResult channelsList();

    // 禁说
    @PostMapping("uuid_mute")
    public ApiResult uuidMute(@RequestParam("uuid")String uuid);

    @PostMapping("uuid_unmute")
    public ApiResult uuidUnmute(@RequestParam("uuid")String uuid);

    // 禁听
    @PostMapping("uuid_mute_hear")
    public ApiResult uuidMuteHear(@RequestParam("uuid")String uuid);

    @PostMapping("uuid_unmute_hear")
    public ApiResult uuidUnmuteHear(@RequestParam("uuid")String uuid);

    @PostMapping("uuid_setvar")
    public ApiResult uuidSetvar(@RequestParam("uuid")String uuid,@RequestParam("key")String key,@RequestParam("val")String val);

    @PostMapping("uuid_setvar_multi")
    public ApiResult uuidSetvarMulti(@RequestParam("uuid")String uuid,@RequestBody Map map);

    @PostMapping("uuid_setvar_multi/async")
    public ApiResult uuidSetvarMultiAsync(@RequestParam("uuid")String uuid,@RequestBody Map map);



}
