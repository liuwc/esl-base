package com.huilan.base.esl.api;

import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.chat.ChatPo;
import com.huilan.base.esl.pojo.po.record.RecordPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Description  chat 消息
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 9:35
*/
@FeignClient("esl-service")
@RequestMapping("esl/api/record")
public interface RecordApi {

    /**
     * @Description  
     * @param recordPo
     * @Return com.huilan.base.esl.pojo.ApiResult 
     * @Author liuwc
     * @Date 2021/8/10 9:35
    */
    @PostMapping("record")
    public ApiResult record(@RequestBody RecordPo recordPo);
    

}
