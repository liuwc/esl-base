package com.huilan.base.esl.api;

import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.api.ApiPo;
import com.huilan.base.esl.pojo.po.call.CallPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Description 呼叫api
 * 说明
 *  1. 呼叫api只能异步执行
 *  2. 所有的方法 *Async 代表是一个异步方法
 *  3. 异步方法返回的结果是 Job-UUID  在BackgroundJob事件中的事件标识
 *
 *
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 13:26
*/
@FeignClient("esl-service")
@RequestMapping("esl/api/call")
public interface CallApi {


    @PostMapping("call/async")
    public ApiResult callAsync(@RequestBody CallPo callPo);



    // 呼叫分机-----------------------------------------------


    /**
     * @Description   所有的呼叫都是异步执行
     * @param user
     * @Return Job-UUID   当前操作的job-uuid 在BackgroundJob事件中可以获取真实结果
     * @Author liuwc
     * @Date 2021/3/21 15:36
    */
    // 呼一个分机
    // originate user/1001 &pack
    @PostMapping("callUser/async")
    public ApiResult callUserAsync(@RequestParam("user")String user);

    // 呼叫一个分机
    // originate  {}user/1001 &pack
    @PostMapping("callUserWithRecord/async")
    public ApiResult callUserWithRecordAsync(@RequestParam("user")String user,@RequestParam("recordAddress") String recordAddress);

    // 呼叫一个分机 桥接 分机2
    // originate user/1001 &bridge(user/1002)
    @PostMapping("callUser2User/async")
    public ApiResult callUser2UserAsync(@RequestParam("userA")String userA,@RequestParam("userB")String userB);

    // 通道变量 ChannelVariableBuild.buildCustomAndRecord
    @PostMapping("callUser2UserWithChannelVariableAsync/async")
    public ApiResult callUser2UserWithChannelVariableAsync(@RequestParam("userA")String userA,@RequestParam("userB")String userB,@RequestBody Map<String,String> channelVariable);

    // 通道变量 ChannelVariableBuild.buildCustomAndRecord
    @PostMapping("callUser2UserWithChannelVariable2Async/async")
    public ApiResult callUser2UserWithChannelVariable2Async(@RequestParam("userA")String userA,@RequestParam("userB")String userB,@RequestBody Map<String,Map<String,String>> channelVariable);


    @PostMapping("callUser2UserWithRecord/async")
    public ApiResult callUser2UserWithRecordAsync(@RequestParam("userA")String userA,@RequestParam("userB")String userB,@RequestParam("recordAddress") String recordAddress);

    // 呼叫一个分机 桥接 外线
    // originate user/1001 &bridge(sofia/gateway/gw1/15566668888)
    // callStr:gw1/15566668888
    @PostMapping("callUser2Phone/async")
    public ApiResult callUser2PhoneAsync(@RequestParam("user")String user,@RequestParam("callStr")String callStr);

    @PostMapping("callUser2PhoneWithRecord/async")
    public ApiResult callUser2PhoneWithRecordAsync(@RequestParam("user")String user,@RequestParam("callStr")String callStr,@RequestParam("recordAddress") String recordAddress);




    // 呼叫外线-----------------------------------------------

    // 呼叫一个外线
    // originate sofia/gateway/gw1/15566668888 &pack
    // callStr:gw1/15566668888
    @PostMapping("callPhone/async")
    public ApiResult callPhoneAsync(@RequestParam("callStr")String callStr);

    @PostMapping("callPhoneWithRecord/async")
    public ApiResult callPhoneWithRecordAsync(@RequestParam("callStr")String callStr,@RequestParam("recordAddress") String recordAddress);

    // 呼叫一个外线 桥接分机
    // originate sofia/gateway/gw1/15566668888 &bridge(sofia/gateway/gw2/16688885555)
    @PostMapping("callPhone2Phone/async")
    public ApiResult callPhone2PhoneAsync(@RequestParam("callStrA")String callStrA,@RequestParam("callStrB")String callStrB);

    @PostMapping("callPhone2PhoneWithRecord/async")
    public ApiResult callPhone2PhoneWithRecordAsync(@RequestParam("callStrA")String callStrA,@RequestParam("callStrB")String callStrB,@RequestParam("recordAddress") String recordAddress);

    // 呼叫一个外线 桥接分机
    // originate sofia/gateway/gw1/15566668888 &bridge(user/1001)
    @PostMapping("callPhone2User/async")
    public ApiResult callPhone2UserAsync(@RequestParam("callStr")String callStr,@RequestParam("user")String user);

    @PostMapping("callPhone2UserWithRecord/async")
    public ApiResult callPhone2UserWithRecordAsync(@RequestParam("callStr")String callStr,@RequestParam("user")String user,@RequestParam("recordAddress") String recordAddress);




}
