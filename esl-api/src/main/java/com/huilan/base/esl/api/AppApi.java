package com.huilan.base.esl.api;


import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.app.AppPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description   执行一个app
 * 说明
 * 1. 所有的app都是同步执行的
 * 2. 所有的app都需要一个 uuid
 * 3. app 是在会话过程中执行的
 *
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 9:35
*/
@FeignClient("esl-service")
@RequestMapping("esl/api/app")
public interface AppApi {


    // 执行一个app
    @PostMapping("app")
    public ApiResult app(@RequestBody AppPo appPo);

    @PostMapping("answer")
    public ApiResult answer(@RequestParam("uuid") String uuid);

    //  uuid bridge 1005
    @PostMapping("bridge")
    public ApiResult bridge(@RequestParam("uuid") String uuid,@RequestParam("user") String user);

    @PostMapping("hangup")
    public ApiResult hangup(@RequestParam("uuid") String uuid);

    // 会话保持
    @PostMapping("hold")
    public ApiResult hold(@RequestParam("uuid") String uuid);

    @PostMapping("unhold")
    public ApiResult unhold(@RequestParam("uuid") String uuid);

    // 执行lua脚本
    @PostMapping("lua")
    public ApiResult lua(@RequestParam("uuid") String uuid,@RequestParam("fileAddress") String fileAddress);

    // 设置一个通道变量
    // arg: RECORD_STEREO=true
    @PostMapping("set")
    public ApiResult set(@RequestParam("uuid") String uuid,@RequestParam("arg") String arg);

    // arg: RECORD_STEREO
    @PostMapping("unset")
    public ApiResult unset(@RequestParam("uuid") String uuid,@RequestParam("arg") String arg);

    // 设置多个通道变量
    // arg: RECORD_STEREO=true RECORD_STEREO_SWAP=true
    @PostMapping("multiset")
    public ApiResult multiset(@RequestParam("uuid") String uuid,@RequestParam("arg") String arg);

    @PostMapping("park")
    public ApiResult park(@RequestParam("uuid") String uuid);

    // 播放一个文件
    @PostMapping("playback")
    public ApiResult playback(@RequestParam("uuid") String uuid,@RequestParam("fileAddress") String fileAddress);

    // 同步录音
    @PostMapping("record")
    public ApiResult record(@RequestParam("uuid") String uuid,@RequestParam("recordAddress") String recordAddress);

    // 异步录音
    @PostMapping("record_session")
    public ApiResult record_session(@RequestParam("uuid") String uuid,@RequestParam("recordAddress") String recordAddress);

    // 异步录音停止
    @PostMapping("stop_record_session")
    public ApiResult stop_record_session(@RequestParam("uuid") String uuid,@RequestParam("recordAddress") String recordAddress);

    // 强插 将uuid和uuidB连接 原来的腿自动挂掉
    @PostMapping("intercept")
    public ApiResult intercept(@RequestParam("uuid") String uuid,@RequestParam("uuidB") String uuidB);


}
