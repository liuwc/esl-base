package com.huilan.base.esl.api;

import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.chat.ChatPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Description  chat 消息
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 9:35
*/
@FeignClient("esl-service")
@RequestMapping("esl/api/chat")
public interface ChatApi {

    /**
     * @Description
     * @param chatPo
     * Display a text message on a Snom 370 or Snom 820
     * The message must be of type "text/plain".
     * @Return com.huilan.base.esl.pojo.ApiResult
     * @Author liuwc
     * @Date 2021/3/11 15:35
    */
    @PostMapping("send")
    public ApiResult send(@RequestBody ChatPo chatPo);
    @PostMapping("send/async")
    public ApiResult sendAsync(@RequestBody ChatPo chatPo);


    @PostMapping("sendMulti")
    public ApiResult sendMulti(@RequestBody List<ChatPo> chatPos);
    @PostMapping("sendMulti/async")
    public ApiResult sendMultiAsync(@RequestBody List<ChatPo> chatPos);


}
