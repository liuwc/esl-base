package com.huilan.base.esl.api;


import com.huilan.base.esl.pojo.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;


/**
 * @Description  会议 api
 * 1. 会议的操作基于会议id conferenceId  目前不提供自定义conferenceId
 * 2. createAsync 接口返回 conferenceId
 * 3. 成员操作基于成员id  memberId
 * 4. memberId 和分机号关系  可以从 listShowMember方法中获取
 *
 * @Return
 * @Author liuwc
 * @Date 2021/3/20 9:35
*/
@FeignClient("esl-service")
@RequestMapping("esl/api/conference")
public interface ConferenceApi {

    @PostMapping("api")
    public ApiResult api(@RequestParam("command") String command);
    @PostMapping("api/async")
    public ApiResult apiAsync(@RequestParam("command") String command);

    /**
     * @Description   创建一个默认的语音会议
     * @param user      分机号
     * @Return conferenceId   返回会议id
     * @Return Job-UUID   当前操作的job-uuid 在BackgroundJob事件中可以获取真实结果
     * @Author liuwc
     * @Date 2021/3/21 14:38
    */
    @PostMapping("create/async")
    public ApiResult createAsync(@RequestParam("user") String user,@RequestParam("id") String id);

    @PostMapping("createWithChannelVariable/async")
    public ApiResult createWithChannelVariableAsync(@RequestParam("user") String user,@RequestBody Map<String,Map<String,String>> map,@RequestParam("id") String id);

    // 将分机加入会议
    @PostMapping("add/async")
    public ApiResult addAsync(@RequestParam("user") String user,@RequestParam("conferenceId")String conferenceId);

    // 将分机加入会议
    @PostMapping("addWithChannelVariable/async")
    public ApiResult addWithChannelVariableAsync(@RequestParam("user") String user,@RequestParam("conferenceId")String conferenceId,@RequestBody Map<String,Map<String,String>> map);

    // 将分机加入会议 批量
    @PostMapping("adds/async")
    public ApiResult addsAsync(@RequestParam("users") List<String> user, @RequestParam("conferenceId")String conferenceId);

    // 将分机加入会议 批量
    @PostMapping("addsWithChannelVariable/async")
    public ApiResult addsWithChannelVariableAsync(@RequestParam("users") List<String> user, @RequestParam("conferenceId")String conferenceId,@RequestBody Map<String,Map<String,String>> map);


    @PostMapping("createWithPassword/async")
    public ApiResult createWithPasswordAsync(@RequestParam("user") String user,@RequestParam("password") String password);

    // 关闭会议
    // 1.查询所有成员
    // 2.循环 hup
    @PostMapping("close")
    public ApiResult close(@RequestParam("conferenceId")String conferenceId);

    // 查看会议成员列表
    @PostMapping("list")
    public ApiResult list(@RequestParam("conferenceId")String conferenceId);

    // 查看会议成员列表
    // {
    //      "1015":member,
    //      "1016":member
    // }
    @PostMapping("listShowMember")
    public ApiResult listShowMember(@RequestParam("conferenceId")String conferenceId);

    // 查看所有的会议
    @PostMapping("listAll")
    public ApiResult list();

    // 踢人
    @PostMapping("kick")
    public ApiResult kick(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);
    // 挂人
    @PostMapping("hup")
    public ApiResult hup(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);
    // 静音
    @PostMapping("mute")
    public ApiResult mute(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);
    // 切换静音
    @PostMapping("tmute")
    public ApiResult tmute(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);
    // 取消静音
    @PostMapping("unmute")
    public ApiResult unmute(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);

    // 关闭摄像头
    @PostMapping("vmute")
    public ApiResult vmute(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);
    // 切换摄像头
    @PostMapping("tvmute")
    public ApiResult tvmute(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);
    // 打开摄像头
    @PostMapping("unvmute")
    public ApiResult unvmute(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);

    // 静音
    @PostMapping("muteAll")
    public ApiResult muteAll(@RequestParam("conferenceId")String conferenceId);
    // 取消静音
    @PostMapping("unmuteAll")
    public ApiResult unmuteAll(@RequestParam("conferenceId")String conferenceId);

    // 存在
    @PostMapping("exist")
    public ApiResult exist(@RequestParam("conferenceId")String conferenceId);

    // 成员存在
    @PostMapping("menberExist")
    public ApiResult menberExist(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);

    // 布局 ConferenceLayoutEnum
    @PostMapping("layout")
    public ApiResult layout(@RequestParam("conferenceId")String conferenceId,@RequestParam("layout")String layout);

    // vidFloor 设置floor
    @PostMapping("vidFloor")
    public ApiResult vidFloor(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);

    // clear_vid_floor 清除 floor
    @PostMapping("clearVidFloor")
    public ApiResult clearVidFloor(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);

    // record
    @PostMapping("record")
    public ApiResult record(@RequestParam("conferenceId")String conferenceId,@RequestParam("filePath")String filePath);

    // vidMuteImg 关闭摄像头 显示的背景图
    @PostMapping("vidMuteImg")
    public ApiResult vidMuteImg(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId,@RequestParam("imgPath")String imgPath);

    // vidMuteImgClear
    @PostMapping("vidMuteImgClear")
    public ApiResult vidMuteImgClear(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);

    // vidLogoImg 设置logo图
    @PostMapping("vidLogoImg")
    public ApiResult vidLogoImg(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId,@RequestParam("imgPath")String imgPath);

    // vidLogoImgClear
    @PostMapping("vidLogoImgClear")
    public ApiResult vidLogoImgClear(@RequestParam("conferenceId")String conferenceId,@RequestParam("memberId")String memberId);

    // vidWritePng 快照
    @PostMapping("vidWritePng")
    public ApiResult vidWritePng(@RequestParam("conferenceId")String conferenceId,@RequestParam("imgPath")String imgPath);



}
