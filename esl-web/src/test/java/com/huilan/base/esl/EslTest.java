package com.huilan.base.esl;

import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.math.MathUtil;
import com.huilan.base.esl.service.fs.EslClient;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EslApplication.class)
//@Ignore
public class EslTest {

    @Autowired
    public EslClient eslClient;

    @Test
    public void test(){

//        for (int i = 0; i < 100; i++) {
//            Executors.newFixedThreadPool(10).execute(()->{
//                api("show calls");
//            });
//            Executors.newFixedThreadPool(10).execute(()->{
//                api("show application");
//            });
//
//        }
//        try {
//            Thread.sleep(30*1000);
//        }catch (Exception e){
//        }
//        log.info("over");
    }

    private void api(String api){
        TimeInterval interval = new TimeInterval();
        interval.start();
        int result  = eslClient.getClient().sendAsyncApiCommand(api, "", false).toString().length();
        log.info("{}:{} time:{}",api,result,interval.intervalMs());
    }


//    public static void main(String[] args) {
//        int a[] = {1,5,6,4,9,-1,3,0};
////        int[] ints = twoSum(a, 9);
////        System.out.println(ints[0]+" "+ints[1]);
//        List<List<Integer>> lists = threeSum(a);
//        log.info("lists:{}",lists);
//    }
//    public static int[] twoSum(int[] nums, int target) {
//        HashMap<Integer, Integer> map = new HashMap<>();
//        for (int i = 0; i < nums.length; i++) {
//            int num = nums[i];
//            if (num >= target){
//                continue;
//            }
//            int kk = target - num;
//            if (map.containsKey(kk)){
//                Integer integer = map.get(kk);
//                return new int[]{integer,i};
//            }
//            map.put(num,i);
//        }
//        return new int[]{0};
//    }
//    public static List<List<Integer>> threeSum(int[] nums) {
//        HashMap<Integer, Integer> map = new HashMap<>();
//        for (int i = 0; i < nums.length; i++) {
//            map.put(nums[i],i);
//        }
//        for (int i = 0; i < nums.length; i++) {
//            for (int j = 1; j < nums.length; j++) {
//                int i1 = nums[i] + nums[j];
//                if (map.containsKey(0-i1)){
//
//                }
//            }
//        }
//
//
//    }


    public static void test_flatMap(){
        var set = Stream.of("My", "Mine")
                .flatMap(str -> str.chars().mapToObj(i -> (char) i))
                .collect(Collectors.toSet());
        System.out.println(set);
    }

    public static void main(String[] args) {
//        test_flatMap();
        callLoseRate();
    }
    public static void callLoseRate(){
        log.info("callLoseRate: begin");
        String connectRate = "0.1";
        int callConcurrent =  20;
        for (int i = callConcurrent; i > 0; i--) {
            callLoseRate(connectRate, i);
        }
        log.info("callLoseRate: end");
    }

    public static void callLoseRate(String connectRate,int callConcurrent){
        // 所有坐席都空闲
        String allRestRate = "";
        // 正常接听一个电话
        String normalWorkRate = "";
        // 呼损
        String callLoseRate = "";

        BigDecimal connectRateBigD = new BigDecimal(connectRate);
        BigDecimal one = new BigDecimal(1);
        // 挂机率
        BigDecimal subtract = one.subtract(connectRateBigD);

        allRestRate = subtract.pow(callConcurrent).setScale(2, RoundingMode.HALF_UP).toPlainString();
        normalWorkRate = connectRateBigD.multiply(subtract.pow(callConcurrent - 1)).multiply(new BigDecimal(callConcurrent)).setScale(2,RoundingMode.HALF_UP).toPlainString();
        callLoseRate = one.subtract(new BigDecimal(allRestRate)).subtract(new BigDecimal(normalWorkRate)).setScale(2,RoundingMode.HALF_UP).toPlainString();

        log.info("接通率:{} 呼叫并发:{} 空闲率:{} 正常率:{} 呼损概率:{}",connectRate,callConcurrent,allRestRate,normalWorkRate,callLoseRate);
    }


}
