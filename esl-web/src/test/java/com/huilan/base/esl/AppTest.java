package com.huilan.base.esl;

import com.huilan.base.esl.api.AppApi;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class AppTest extends EslTest {

    @Autowired
    private AppApi appApi;

    private static String uuid ="3d599b37-e0b4-48d6-a623-6fb33e249d85";

    @Test
    public void hold(){
        appApi.hold(uuid);
    }
    @Test
    public void unhold(){
        appApi.unhold(uuid);
    }
    @Test
    public void hangup(){
        appApi.hangup(uuid);
    }
    @Test
    public void bridge(){
        appApi.bridge("e84517d1-ff83-4a8c-83bf-8c58429924d4","1002");
    }
    @Test
    public void intercept(){
        appApi.intercept("cdab9acf-1de7-44e9-aa49-e36d2f88e6bb","e84517d1-ff83-4a8c-83bf-8c58429924d4");
    }
    @Test
    public void answer(){
        appApi.answer("1aec76e9-4599-422b-8842-9b45ae8c9fcd");
    }

    @Test
    public void unset(){
        appApi.unset("a16658dd-f00c-449c-a4df-5ce76463c836","absolute_codec_string");
    }



}
