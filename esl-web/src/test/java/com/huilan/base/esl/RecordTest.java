package com.huilan.base.esl;

import com.huilan.base.esl.api.ApiApi;
import com.huilan.base.esl.api.RecordApi;
import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.api.ApiEnum;
import com.huilan.base.esl.pojo.po.api.ApiPo;
import com.huilan.base.esl.pojo.po.record.RecordChannelVariable;
import com.huilan.base.esl.pojo.po.record.RecordPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Slf4j
public class RecordTest extends EslTest{

    @Autowired
    private RecordApi recordApi;

    @Test
    public void Record(){
        log.info("begin");
        ApiResult result = recordApi.record(RecordPo
                .builder()
                .uuid("uuid")
                .recordAddress("/opt/aicc/test.mp3")
                // 开启异步录音
                .isRecordSession(true)
                // 设置通道变量
                .recordChannelVariable(RecordChannelVariable
                        .builder()
                        //开启追加
                        .record_append(true)
                        // 关闭立体声
                        .record_stereo(false)
                        // 只录读方向 fs听到的声音
                        .record_read_only(true)
                        .build())
                .build());
        log.info("ApiResult:{}",result);
    }

    public static void main(String[] args) {
        log.info("begin");
        Map<String, Object> map = RecordChannelVariable
                .builder()
                .record_answer_req(true)
                .record_append(true)
                .record_min_sec(2)
                .build()
                .transformChannelVariableMap();
        log.info("map:{}",map);


    }





}
