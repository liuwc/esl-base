package com.huilan.base.esl;

import com.huilan.base.esl.service.acd.domain.model.agent.Agent;
import com.huilan.base.esl.service.acd.domain.model.phoneinline.Channel;
import com.huilan.base.esl.service.acd.domain.model.phoneinline.PhoneInline;
import com.huilan.base.esl.service.acd.domain.model.queue.Queue;
import com.huilan.base.esl.service.acd.domain.model.queue.QueueACDEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class TransferTest extends EslTest {

    @Autowired
    private RedissonClient redisson;

    private static String queueId = "666";
//    private static String agentEtx = "1003";

    @Test
    public void createQueue(){
        log.info("创建队列");
        Queue.create(queueId, QueueACDEnum.loop);
    }
    @Test
    public void getQueue(){
        log.info("get队列");
        Queue queue = Queue.get(queueId);
        log.info("queueId:{}",queue.getId());
    }

    @Test
    public void createAgent(){
        createAgent("1003");
        createAgent("1004");
        createAgent("1005");
    }
    public void createAgent(String ext){
        log.info("创建坐席");
        Agent.create( ext, ext);
    }
    public void agentIntoQueue(){
        agentIntoQueue("1003");
        agentIntoQueue("1004");
        agentIntoQueue("1005");
    }
    public void agentIntoQueue(String agentId){
        Agent agent = Agent.get(agentId);
        agent.agentIntoQueue(agentId,queueId);
    }

    @Test
    public void agentOnline(){
        agentOnline("1003");
        agentOnline("1004");
        agentOnline("1005");
    }
    public void agentOnline(String agentId){
        log.info("坐席上线");
        Agent agent = Agent.get(agentId);
        agent.online();
    }
    @Test
    public void agentRest(){
        agentRest("1003");
        agentRest("1004");
//        agentRest("1005");
    }
    public void agentRest(String agentId){
        log.info("坐席休息");
        Agent agent = Agent.get(agentId);
        agent.rest();
    }

//    public void agentOnlineAsync(){
//        Executors.newSingleThreadExecutor().execute(()->{
//            try {
//                TimeUnit.SECONDS.sleep(10);
//                log.info("模拟坐席上线 sleep 10s");
//                agentOnline();
//            }catch (Exception e){}
//        });
//    }

    @Test
    public void transfer(){
        log.info("来电转接");
        String phone = "1551002";
        String uuid = "80ac393c-eda1-46d1-baec-5d374beae0f9";
        Queue queue = Queue.get(queueId);
        PhoneInline phoneInline = PhoneInline.create(phone, new Channel(uuid), queue);
        phoneInline.transfer();
    }

    @Test
    public void testApi(){
        createQueue();
        createAgent();
        agentIntoQueue();
        agentOnline();
//        agentOnlineAsync();
//        transfer();
    }

}
