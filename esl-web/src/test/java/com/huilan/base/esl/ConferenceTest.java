package com.huilan.base.esl;

import com.huilan.base.esl.api.CallApi;
import com.huilan.base.esl.api.ConferenceApi;
import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.conference.ConferenceLayoutEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class ConferenceTest extends EslTest {

    @Autowired
    private ConferenceApi conferenceApi;

    @Test
    public void create(){
//        conferenceApi.createAsync("1001");
    }
    @Test
    public void add(){
//        conferenceApi.addAsync("1001","10");
    }
    @Test
    public void list(){
        conferenceApi.list("9");
    }
    @Test
    public void listAll(){
        conferenceApi.list();
    }
    @Test
    public void listShowNumber(){
        conferenceApi.listShowMember("9");
    }
    @Test
    public void kick(){
        conferenceApi.kick("9","13");
        conferenceApi.kick("9","14");
    }
    @Test
    public void hup(){
        conferenceApi.hup("10","15");
//        conferenceApi.kick("10","16");
    }
    @Test
    public void layout(){
        conferenceApi.layout("10", ConferenceLayoutEnum.layout_1x1.name());
    }

    @Test
    public void vidFloor(){
        conferenceApi.vidFloor("10", "20");
        conferenceApi.clearVidFloor("10", "20");
    }












}
