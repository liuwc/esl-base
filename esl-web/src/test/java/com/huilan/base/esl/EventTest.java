package com.huilan.base.esl;

import com.huilan.base.esl.api.EventApi;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class EventTest extends EslTest {

    @Autowired
    private EventApi eventApi;

    @Test
    public void send_message(){
        eventApi.send_message("1006","测试消息");
    }


}
