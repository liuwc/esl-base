package com.huilan.base.esl;

import com.huilan.base.esl.api.ApiApi;
import com.huilan.base.esl.api.AppApi;
import com.huilan.base.esl.api.CallApi;
import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.api.ApiPo;
import com.huilan.base.esl.pojo.po.app.AppPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class CallTest extends EslTest {

    @Autowired
    private CallApi callApi;

    @Autowired
    private AppApi appApi;
    @Autowired
    private ApiApi apiApi;

    @Test
    public void callUser2User(){
        callApi.callUser2UserAsync("1019","1009");
    }
    @Test
    public void callUser(){

        apiApi.api(ApiPo.builder().build());
    }
    @Test
    public void callUser2(){
        log.info("开始");
        ApiResult playtts = appApi.app(AppPo.builder().name("playtts").uuid("1cdcf84c-df9f-11eb-b6aa-a3804d77fc34").arg("你好，您拨打的电话忙，别再打了").build());
        log.info("playtts :{}",playtts);
    }

    @Test
    public void callUser2UserWithRecord(){
        callApi.callUser2UserWithRecordAsync("1058","1006","/home/aicc-cloud/tmp/aaa.wav");
    }









}
