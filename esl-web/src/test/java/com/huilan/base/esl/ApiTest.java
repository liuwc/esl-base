package com.huilan.base.esl;

import com.huilan.base.esl.api.ApiApi;
import com.huilan.base.esl.api.AppApi;
import com.huilan.base.esl.pojo.ApiResult;
import com.huilan.base.esl.pojo.po.api.ApiEnum;
import com.huilan.base.esl.pojo.po.api.ApiPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class ApiTest extends EslTest {

    @Autowired
    private ApiApi apiApi;

    @Test
    public void uuid_bridge(){

        apiApi.api(ApiPo.builder()
                .name(ApiEnum.uuid_bridge.name())
                .arg("77ee4fc5-7716-4cd9-859e-3c1ae5b3f925 c328b019-c8b7-420b-8a15-932e9e4ad646")
                .build());
    }






}
