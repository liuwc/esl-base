package com.huilan.base.esl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
// 扫描所有包以及相关组件包
@ComponentScan(basePackages = {"com.huilan"})
// eureka 注册
//@EnableDiscoveryClient
public class EslApplication {
    public static void main(String[] args) {
        SpringApplication.run(EslApplication.class, args);
    }
}
